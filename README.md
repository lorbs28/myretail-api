# myRetail API Case Study

This is the project that I wrote for the myRetail API case study.  The following are some of the technologies used:

* Java 8
* Gradle
* Spring Boot
* Jersey
* MongoDB
* Mockito
* Log4j

## Prerequisites

Make sure you have Gradle installed.  Installation instructions can be found here: https://gradle.org/install/

## 1.) Getting the project

1.) In a directory where you want to put the project, run the following command in your terminal:

```
git clone https://bitbucket.org/lorbs28/myretail-api.git
```


## 2.) Setting up the MongoDB sample data

1.) Make sure your MongoDB server is on.

2.) Go to the root of the project directory.

3.) Then go to `/setup/myRetail.20171122.mongodb/`.

4.) In your terminal at that location, run the following command:

```
mongorestore
```

*Note:* this will restore the demo's collection along with the required user in "System".

## 3.) Setting up and starting the API

1.) Go to the root of the project directory.

2.) Run the following commands:

```
./gradlew build && gradle bootRun
```

*Note:* this will build the project, downloading any necessary dependencies, and then start the Spring Boot app.

## 4.) Accessing the API

Hop on a browser of your choice and go to the following URL: http://localhost:8080/v1/products/13860428
