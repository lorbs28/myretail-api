package com.bryanlor.myretailapi.entities;

import java.math.BigDecimal;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * This class will contain properties for Price.
 * @author lorbs28
 *
 */
public class Price {
	private BigDecimal value;
	
	@Field(value = "currency_code")
	private String currencyCode;
	
	public BigDecimal getValue() {
		return value;
	}
	
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	@Override
	public String toString() {
		return String.format("Price[value=%f, currencyCode=%s]", this.value, this.currencyCode);
	}
}
