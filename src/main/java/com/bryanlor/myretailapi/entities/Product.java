package com.bryanlor.myretailapi.entities;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * This class will contain properties for Product.
 * 
 * The MongoDB collection is "products".
 * 
 * Fields ignored:
 * - name (pulled from external API call)s
 * 
 * @author lorbs28
 *
 */
@Document(collection = "products")
public class Product {

	@Id
	private String id;
	
	@Field(value = "product_id")
	private int productId;
	
	@Field(value = "current_price")
	private List<Price> currentPrice;
	
	@Transient
	private String name;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public int getProductId() {
		return productId;
	}
	
	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	public List<Price> getCurrentPrice() {
		return currentPrice;
	}
	
	public void setCurrentPrice(List<Price> currentPrice) {
		this.currentPrice = currentPrice;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("Product[id=%s, productId=%s, price=%s, name=%s]", this.id, this.productId, this.currentPrice.toString(), this.name);
	}
}
