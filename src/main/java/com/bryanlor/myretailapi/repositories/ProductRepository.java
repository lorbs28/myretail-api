package com.bryanlor.myretailapi.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bryanlor.myretailapi.entities.Product;

public interface ProductRepository extends MongoRepository<Product, String>, ProductRepositoryCustom {
	public Product findByProductId(int productId);
}
