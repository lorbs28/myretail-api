package com.bryanlor.myretailapi.repositories;

import com.bryanlor.myretailapi.entities.Product;

public interface ProductRepositoryCustom {
	Product updateAddProductPrice(Product product);
}
