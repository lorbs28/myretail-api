package com.bryanlor.myretailapi.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.bryanlor.myretailapi.entities.Product;

public class ProductRepositoryImpl implements ProductRepositoryCustom {
	
	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public Product updateAddProductPrice(Product updatedProduct) {
		Query query = new Query(Criteria.where("product_id").is(updatedProduct.getProductId()));
		
		Update update = new Update();
		update.set("current_price", updatedProduct.getCurrentPrice());
		
		return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Product.class);
	}

}
