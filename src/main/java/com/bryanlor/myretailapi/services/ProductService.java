package com.bryanlor.myretailapi.services;

import java.io.IOException;

import com.bryanlor.myretailapi.entities.Product;

public interface ProductService {
	public Product retrieveProductById(int productId) throws IOException;
	public Product updateAddProductPrice(int productId, String price, String currencyCode);
}
