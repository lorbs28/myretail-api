package com.bryanlor.myretailapi.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.bryanlor.myretailapi.entities.Price;
import com.bryanlor.myretailapi.entities.Product;
import com.bryanlor.myretailapi.repositories.ProductRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@PropertySource("classpath:externalApi.properties")
public class ProductServiceImpl implements ProductService {
	
	private ProductRepository productRepository;
	
	@Value("${uri.redskyTarget}")
	private String redskyTargetUri;
	
	private static final Logger log = Logger.getLogger(ProductServiceImpl.class);
	
	@Autowired
	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}
	
	/**
	 * This method will retrieve the product, given a product id.
	 */
	@Override
	public Product retrieveProductById(int productId) {
    	// Grab the product information from our local datastore using the product id.
    	Product product = productRepository.findByProductId(productId);
    	
    	// If the product was in our local datastore, then set the
    	// product name and all other info.
    	if (product != null) {
    		product = this.retrieveExternalProductInformation(product);
    	}
    	
		
		return product;
	}
	
	/**
	 * This method will retrieve the product name from an external API, given product id.
	 * 
	 * @param productId
	 * @return
	 */
	protected Product retrieveExternalProductInformation(Product product) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			// Set the URI for redsky.target.com and the parameters
	    	final String redskyUriWithProductId = String.format(redskyTargetUri, product.getProductId());
	    	
	    	// Call the external API using RestTemplate object, returning the response as a string.
	    	Client client = ClientBuilder.newClient();
	    	Response clientResponse = client.target(redskyUriWithProductId)
	    		.request()
	    		.accept(MediaType.APPLICATION_JSON)
	    		.get();
	    	
	    	// Read the response entity as a string so that we can change it into a JsonNode later.
	    	String response = clientResponse.readEntity(String.class);
	    	
			// Convert the response string into a JsonNode so we can traverse it for the appropriate values.
	    	if (StringUtils.isNotBlank(response)) {
				JsonNode redskyObj = mapper.readTree(response);
				
				// Get the product name.
				product.setName(this.pullProductName(redskyObj));
	    	}
	    	
		} catch (IOException e) {
			log.error("Error occurred when retrieving product info from external API: ", e);
		}
		
		return product;
	}
	
	/**
	 * This method will pull the product name from the JSON response from the 
	 * external API.
	 * 
	 * @param jsonNode
	 * @return
	 */
	protected String pullProductName(JsonNode jsonNode) {
		JsonNode itemNode = jsonNode.get("product").get("item");
		String productName = "";
		
		if (itemNode.get("product_description") != null) {
	    	// Set any additional info into the Product object.
	    	productName = itemNode.get("product_description").get("title").textValue();
		}
		
		return productName;
	}
	
	/**
	 * This method will update the product price, give the product id and price.
	 */
	@Override
	public Product updateAddProductPrice(int productId, String newPriceValue, String currencyCode) {
		// Get the current product data in our local store.
		Product product = productRepository.findByProductId(productId);
		
		// If the product object is not null, meaning it exists in our local datastore,
		// then proceed to update the price.
		if (product != null) {
			
			boolean priceExistsForCurrency = false;
			List<Price> priceList = product.getCurrentPrice();
			
			// Setup the new price
			Price newPrice = new Price();
			newPrice.setValue(new BigDecimal(newPriceValue));
			newPrice.setCurrencyCode(StringUtils.upperCase(currencyCode));
			
			for (int i = 0; i < priceList.size(); i++) {
				String currentCurrencyCode = priceList.get(i).getCurrencyCode();
				
				// Check the incoming currency code against the current currency code in
				// the loop, ignoring the case.  If they match, then update the price for that currency code.
				if ((currencyCode).equalsIgnoreCase(currentCurrencyCode)) {
					priceList.set(i, newPrice);
					priceExistsForCurrency = true;
					break;
				}
			}
			
			// If the request currency code doesn't exist yet, then add it along with the price.
			if (!priceExistsForCurrency) {
				priceList.add(newPrice);
			}
			
			product.setCurrentPrice(priceList);
			
			product = productRepository.updateAddProductPrice(product);
		}
		
		return product;
	}
}
