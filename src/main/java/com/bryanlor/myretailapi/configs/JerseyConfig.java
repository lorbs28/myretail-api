package com.bryanlor.myretailapi.configs;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.bryanlor.myretailapi.endpoints.ProductEndpoint;

@Component
@ApplicationPath("/v1")
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		register(ProductEndpoint.class);
	}
} 
