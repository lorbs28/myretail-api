package com.bryanlor.myretailapi.endpoints;

import java.io.IOException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bryanlor.myretailapi.entities.Product;
import com.bryanlor.myretailapi.exceptions.NotAuthorizedException;
import com.bryanlor.myretailapi.services.ProductService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component	
@Path("/products")
public class ProductEndpoint {
	
	@Autowired
	private ProductService productService;
	
	private static final Logger log = Logger.getLogger(ProductEndpoint.class);
	private static final String ERROR_KEY = "error";
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductById(@PathParam("id") int id) throws IOException, JSONException {
		Product product = productService.retrieveProductById(id);
		
		if (product == null) {
			JSONObject message = new JSONObject();
			message.put(ERROR_KEY, String.format("Product data not found for id: %d", id));
			return Response.status(Response.Status.NOT_FOUND).entity(message.toString()).build();
		}
		
		return Response.ok(product).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateProductById(@PathParam("id") int id, String request) throws JSONException {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject message = new JSONObject();
		Product product = null;
		String price;
		String currencyCode;
		
		
		// Convert the json string into a json node to pull the values.
		try {
			JsonNode jsonRequest = mapper.readTree(request);
			price = jsonRequest.get("value") != null ? jsonRequest.get("value").textValue() : null;
			currencyCode = jsonRequest.get("currency_code") != null ? jsonRequest.get("currency_code").textValue() : null;
			
			// Check to make sure that we have both value and currency_code keys. If not
			// then return a response.
			if (StringUtils.isBlank(price) || StringUtils.isBlank(currencyCode)) {
				
				// Return a response telling the user that the request data is invalid.
				message.put(ERROR_KEY, String.format("Invalid request data for: %d", id));
				return Response.status(Response.Status.BAD_REQUEST).entity(message.toString()).build();
			}
			
		} catch (IOException e) {
			log.error("Error occurred when trying to convert the request into JSON: ", e);
			
			// Return a response telling the user that the process could not be processed.
			message.put(ERROR_KEY, String.format("Unable to process request for: %d", id));
			return Response.status(Response.Status.BAD_REQUEST).entity(message.toString()).build();
		}
		
		product = productService.updateAddProductPrice(id, price, currencyCode);
		
		// If the product object is null, we want to return a message telling the user
		// that no product data can be found.
		if (product == null) {
			message.put(ERROR_KEY, String.format("Product data not found for id: %d", id));
			return Response.status(Response.Status.NOT_FOUND).entity(message.toString()).build();
		}
		
		return Response.ok(product).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response showWelcomeMessage() {
		throw new NotAuthorizedException("Welcome to the myRetail API for products.  Please refer to the myRetail API documentation for further information.");
	}
	
}
