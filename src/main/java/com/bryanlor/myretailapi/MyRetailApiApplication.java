package com.bryanlor.myretailapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MyRetailApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyRetailApiApplication.class, args);
	}
}
