package com.bryanlor.myretailapi.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.bryanlor.myretailapi.entities.Price;
import com.bryanlor.myretailapi.entities.Product;
import com.bryanlor.myretailapi.repositories.ProductRepository;
import com.bryanlor.myretailapi.services.ProductServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ProductServiceImplTest {
	
	@Mock
	private ProductRepository productRepository;
	
	@InjectMocks
	private ProductServiceImpl productService;
	
	private Product testProduct;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		// Mock our product object with valid data.
		Product product = new Product();
		product.setId("5a134474dc38ce11713da383");
		product.setProductId(13860428);
		product.setName("The Big Lebowski (Blu-ray)");
		
		Price usdPrice = new Price();
		usdPrice.setCurrencyCode("USD");
		usdPrice.setValue(new BigDecimal("14.99"));
		
		List<Price> currentPrice = new ArrayList<Price>();
		currentPrice.add(usdPrice);
		
		product.setCurrentPrice(currentPrice);
		
		// Stub the repository method
		when(productRepository.findByProductId(13860428)).thenReturn(product);
		
		this.testProduct = product;
		
		// Use the reflection test utils to set our property for the external API uri in the ProductServiceImpl class.
		ReflectionTestUtils.setField(productService, "redskyTargetUri", "http://redsky.target.com/v2/pdp/tcin/%d?excludes=taxonomy,price,"
				+ "promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics");
	}
	
	/**
	 * Test the service method to retrieve product by id with a
	 * valid product id.
	 * 
	 */
	@Test
	public void testRetrieveProductByIdWithValidId() {
		Product product = productService.retrieveProductById(13860428);
		
		// Test that the returned product is not null. 
		assertNotNull(product);
		
		// Test that the values matches.
		assertEquals("5a134474dc38ce11713da383", product.getId());
		assertEquals(13860428, product.getProductId());
		assertEquals("The Big Lebowski (Blu-ray)", product.getName());
		assertEquals("USD", product.getCurrentPrice().get(0).getCurrencyCode());
		assertEquals(new BigDecimal("14.99"), product.getCurrentPrice().get(0).getValue());
		
		// Test the toStrings of the product object also
		assertNotNull(product.toString());
		assertNotNull(product.getCurrentPrice().get(0).toString());
		
		assertTrue(product.toString().contains("id=5a134474dc38ce11713da383"));
		assertTrue(product.getCurrentPrice().get(0).toString().contains("currencyCode=USD"));
	}
	
	/**
	 * Test that the service method to retrieve product by id will return
	 * a null if the product does not exist.
	 * 
	 */
	@Test
	public void testRetrieveProductByIdWithInvalidId() {
		// Ensure that a null product object is returned if the product is not found in the datastore.
		assertNull(productService.retrieveProductById(123456));
	}
	
	@Test
	public void testUpdateAddProductPriceWithValidParameters() {
		// Mock our product object with valid data after a price update.
		Product product = this.testProduct;
		
		Price updatedUsdPrice = new Price();
		updatedUsdPrice.setCurrencyCode("USD");
		updatedUsdPrice.setValue(new BigDecimal("9.99"));
		
		List<Price> updatedPrice = new ArrayList<Price>();
		updatedPrice.add(updatedUsdPrice);
		
		product.setCurrentPrice(updatedPrice);
		
		when(productRepository.updateAddProductPrice(this.testProduct)).thenReturn(product);
		
		// Test the return object for null.
		assertNotNull(productService.updateAddProductPrice(13860428, "9.99", "USD"));
		
		// Test that the updated price matches.
		assertEquals(new BigDecimal("9.99"), productService.updateAddProductPrice(13860428, "9.99", "USD").getCurrentPrice().get(0).getValue());
	}
}
