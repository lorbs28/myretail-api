package com.bryanlor.myretailapi.repositories;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bryanlor.myretailapi.entities.Price;
import com.bryanlor.myretailapi.entities.Product;
import com.bryanlor.myretailapi.repositories.ProductRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ProductRepositoryTest {
	
	@Mock
	private ProductRepository productRepository;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		// Mock our product object with some valid data.
		Product product = new Product();
		product.setId("5a134474dc38ce11713da383");
		product.setProductId(13860428);
		product.setName("The Big Lebowski (Blu-ray)");
		
		Price usdPrice = new Price();
		usdPrice.setCurrencyCode("USD");
		usdPrice.setValue(new BigDecimal("14.99"));
		
		List<Price> currentPrice = new ArrayList<Price>();
		currentPrice.add(usdPrice);
		
		product.setCurrentPrice(currentPrice);
		
		// Stub the repository method
		when(productRepository.findByProductId(13860428)).thenReturn(product);
	}

	/**
	 * Test the repository method to find by product id with a valid id.
	 */
	@Test
	public void testFindByProductIdWithValidId() {
		
		// Test that the returned object is not null.
		assertNotNull(productRepository.findByProductId(13860428));
		
		// Test that the returned object is a Product object.
		assertThat(productRepository.findByProductId(13860428), instanceOf(Product.class));
		
		// Test one of the properties to make sure they match
		assertEquals("5a134474dc38ce11713da383", productRepository.findByProductId(13860428).getId());
	}
	
	/**
	 * Test the repository method to find by product id with an invalid id.
	 */
	@Test
	public void testFindByProductIdWithInvalidId() {
		// Stub the repository method
		when(productRepository.findByProductId(12345678)).thenReturn(null);
		
		// Test that the returned object is not null.
		assertNull(productRepository.findByProductId(12345678));
	}
}
