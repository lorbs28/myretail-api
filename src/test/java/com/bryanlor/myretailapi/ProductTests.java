package com.bryanlor.myretailapi;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.bryanlor.myretailapi.endpoints.ProductEndpointTest;
import com.bryanlor.myretailapi.repositories.ProductRepositoryTest;
import com.bryanlor.myretailapi.services.ProductServiceImplTest;

@RunWith(Suite.class)
@SuiteClasses({ 
	MyRetailApiApplicationTests.class,
	ProductEndpointTest.class,
	ProductServiceImplTest.class,
	ProductRepositoryTest.class
})
public class ProductTests {

}
