package com.bryanlor.myretailapi;

import static org.junit.Assert.*;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;

import com.bryanlor.myretailapi.filters.CORSFilter;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class MyRetailApiApplicationTests {

	@Test
	public void contextLoads() {
	}
		
	@Test
    public void testRequestExpectStandardCorsResponse() throws ServletException, IOException {
        CORSFilter corsFilter = new CORSFilter();

        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/v1/products/");
        
        request.addHeader("Origin", "example.com");

        MockHttpServletResponse response = new MockHttpServletResponse();

        FilterChain filterChain = newMockFilterChain();

        corsFilter.doFilter(request, response, filterChain);

        assertEquals("*", response.getHeaderValue("Access-Control-Allow-Origin"));
        
        corsFilter.destroy();
    }
	
	private static FilterChain newMockFilterChain() {
        FilterChain filterChain = new FilterChain() {

            @Override
            public void doFilter(final ServletRequest request, final ServletResponse response)
                    throws IOException,
                    ServletException {
                // Do nothing.
            }

        };
        return filterChain;
    }
}
