package com.bryanlor.myretailapi.endpoints;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bryanlor.myretailapi.endpoints.ProductEndpoint;
import com.bryanlor.myretailapi.entities.Price;
import com.bryanlor.myretailapi.entities.Product;
import com.bryanlor.myretailapi.exceptions.NotAuthorizedException;
import com.bryanlor.myretailapi.services.ProductServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ProductEndpointTest {
	
	@Mock
	private ProductServiceImpl productService;
	
	@InjectMocks
	private ProductEndpoint productEndpoint;
	
	private static final int HTTP_OK = 200;
	private static final int NOT_FOUND = 404;
	private static final int BAD_REQUEST = 400;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		// Mock our product object with valid data.
		Product product = new Product();
		product.setId("5a134474dc38ce11713da383");
		product.setProductId(13860428);
		product.setName("The Big Lebowski (Blu-ray)");
		
		Price usdPrice = new Price();
		usdPrice.setCurrencyCode("USD");
		usdPrice.setValue(new BigDecimal("14.99"));
		
		List<Price> currentPrice = new ArrayList<Price>();
		currentPrice.add(usdPrice);
		
		product.setCurrentPrice(currentPrice);
		
		when(productService.retrieveProductById(13860428)).thenReturn(product);
		
		// Mock our product object with valid data after a price update.
		Price updatedUsdPrice = new Price();
		updatedUsdPrice.setCurrencyCode("USD");
		updatedUsdPrice.setValue(new BigDecimal("13.99"));
		
		List<Price> updatedPrice = new ArrayList<Price>();
		updatedPrice.add(updatedUsdPrice);
		
		product.setCurrentPrice(updatedPrice);
		
		when(productService.updateAddProductPrice(13860428, "13.99", "USD")).thenReturn(product);
	}
	
	/**
	 * Test the endpoint for retrieving a product with a valid id.
	 * @throws Exception
	 */
	@Test
	public void testGetProductByIdWithValidId() throws Exception {
		
		// Test that the returned response object is not null.
		assertNotNull(productEndpoint.getProductById(13860428));
		
		// Test that the returned object is of type Response.
		assertEquals(HTTP_OK, productEndpoint.getProductById(13860428).getStatus());
	}
	
	/**
	 * Test the endpoint for retrieving a product with an invalid id.
	 * @throws Exception
	 */
	@Test
	public void testGetProductByIdWithInvalidId() throws Exception {
		// Test that the returned object is not null.
		assertNotNull(productEndpoint.getProductById(12345));
		
		// Test that response object has a status of 404.
		assertEquals(NOT_FOUND, productEndpoint.getProductById(12345).getStatus());
		
	}
	
	@Test
	public void testUpdateProductByIdWithValidId() throws JSONException {
		String request = "{ \"value\": \"13.99\", \"currency_code\":\"USD\" }";

		// Test that the return object is not null.
		assertNotNull(productEndpoint.updateProductById(13860428, request));
		
		// Test that the response object is a 200, success.
		assertEquals(HTTP_OK, productEndpoint.updateProductById(13860428, request).getStatus());
	}
	
	@Test
	public void testUpdateProductByIdWithValidIdInvalidRequest() throws JSONException {
		// Malformed request missing price value, key and value
		String malformedRequest = "{ \"currency_code\":\"USD\" }";
		
		// Test that the return object is not null.
		assertNotNull(productEndpoint.updateProductById(13860428, malformedRequest));
		
		// Test that the response object is a 400, success.
		assertEquals(BAD_REQUEST, productEndpoint.updateProductById(13860428, malformedRequest).getStatus());
	}
	
	@Test
	public void testUpdateProductByIdWithInvalidId() throws JSONException {
		String request = "{ \"value\": \"13.99\", \"currency_code\":\"USD\" }";
		
		when(productService.updateAddProductPrice(1234567, "13.99", "USD")).thenReturn(null);
		
		// Test that the return object is not null.
		assertNotNull(productEndpoint.updateProductById(1234567, request));
		
		// Test that the response object is a 200, success.
		assertEquals(NOT_FOUND, productEndpoint.updateProductById(1234567, request).getStatus());
	}
	
	@Test(expected = NullPointerException.class)
	public void testUpdateProductByIdIOException() throws JsonProcessingException, IOException, JSONException {
		ObjectMapper mapper = mock(ObjectMapper.class); 
		String request = null;
		
		// When the readTree method gets called, stub the metho, and throw an IOException.
		when(mapper.readTree(request)).thenThrow(new IOException());
		
		// Test that we get a 400 code.
		assertEquals(BAD_REQUEST, productEndpoint.updateProductById(12345678, request).getStatus());
	}
	
	@Test(expected = NotAuthorizedException.class)
	public void testWelcomeMessage() {
		productEndpoint.showWelcomeMessage();
	}
	
}
